function parseHash () {
	window._conf = {
		id: '',
		pwd: '',
		type: '',
		admin: window._conf !== undefined ? _conf.admin : false
	};
	var re = location.hash.match(/^#((?:\/?([1-9]\d*|[bi][0-9a-zA-Z]+))+)(\?(.+))?/);

	if (re) {
		window._conf.id = re[1] || '';
		var first = re[2].substring(0, 1).toLowerCase();
		window._conf.type = first === 'b' || first === 'i' ? first : '';

		if (isEmpty(re[4])) return;

		var query = re[4].split('&');
		for (var i = 0; i < query.length; i++) {
			var a = query[i].split('=');
			if (_conf.hasOwnProperty(a[0])) window._conf[a[0]] = a[1];
		}
	}
}

function buildHash (id, pwd) {
	var hash = isEmpty(id) ? '' : id;
	if (!isEmpty(pwd)) hash += '?pwd=' + pwd;
	return hash;
}

function buildUrl (uri, params) {
	if (varType(params) === 'Object' && Object.keys(params).length > 0) {
		var arr = [];
		for (var key in params) {
			if (!isEmpty(params[key])) arr.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
		}
		if (arr.length > 0) uri += '?' + arr.join('&');
	} else if (varType(params) === 'String') {
		uri += '?' + params;
	}

	return location.href.replace(/[#?].*$/g, '') + uri;
}

layui.config({
	base: "js/lay-module/",
	version: true
}).extend({
	lianyi: 'lianyi'
}).use(['jquery', 'layer', 'table', 'form', 'upload', 'element', 'dropdown', 'util', 'lianyi'], function () {
	var $        = layui.jquery,
		layer    = layui.layer,
		table    = layui.table,
		form     = layui.form,
		upload   = layui.upload,
		element  = layui.element,
		dropdown = layui.dropdown,
		util     = layui.util,
		lianyi   = layui.lianyi;

	window.cut_file = {};
	var tableIns;

	var parseLayer = function (msg) {
		return layer.open({
			title: '涟漪云 <div class="update-time"><b class="layui-font-red">2023-08-11</b></div>',
			type: 1,
			skin: 'layer-form',
			closeBtn: false,
			shadeClose: true,
			content: $('#lanzou'),
			btn: ['确定', '取消'],
			id: 'parseLayer',
			success: function (elem, index) {
				if (msg) lianyi.fail(msg);
				form.val('lanzou', _conf);

				var that = this;
				elem.find('input[name],textarea[name]').off('keydown').on('keydown', function (e) {
					if (e.keyCode === 13) that.yes(index, elem);
				}).trigger('input');
			},
			yes: function (index, elem) {
				var field = form.val('lanzou');

				var re = field.id.match(/^(?:\/?([1-9]\d*|[bi][0-9a-zA-Z]+))+$/);
				if (!isEmpty(re)) {
					var first = re[1].substring(0, 1).toLowerCase();
					if (first === 'b' || first === 'i') field.type = first;
				}

				window._conf = $.extend(_conf, field);

				layer.close(index);
				location.hash = buildHash(field.id, field.pwd);
			}
		});
	}

	var layerLogin = function () {
		if (_conf.admin) return true;

		layer.open({
			title: '管理员登录',
			type: 1,
			skin: 'layer-form',
			shadeClose: true,
			content: $('#layer-login'),
			btn: ['登录', '取消'],
			success: function (elem, index) {
				var that = this;
				elem.find('input[name],textarea[name]').off('keydown').on('keydown', function (e) {
					if (e.keyCode === 13) that.yes(index, elem);
				});
			},
			yes: function (index, elem) {
				form.submit('layer-login', function (data) {
					var field = data.field,
						that  = data.elem;

					lianyi.add_icon(that);
					lianyi.request({
						url: 'api.php?c=login',
						data: field,
						success: function (res) {
							layer.close(index);
							window._conf.admin = true;
						},
						fail: function (res) {
							$(elem).find('.layui-input-code > .layui-input-suffix > img').trigger('click');
						},
						complete: function (res) {
							lianyi.remove_icon(that);
						}
					});
				});
			}
		});
		return false;
	}

	var refresh = function (page) {
		layer.closeAll()

		var where = {id: _conf.id, pwd: _conf.pwd};

		if (table.cache.hasOwnProperty('list'))
			return table.reload('list', {
				where: where,
				page: {
					groups: 1,
					first: '首页',
					last: false,
					layout: ['prev', 'page', 'next', 'skip'],
					curr: page || 1
				},
				limit: _conf.type === '' ? 18 : 50
			});

		tableIns = table.render({
			elem: '#list',
			url: 'api.php?c=list',
			method: 'POST',
			where: where,
			toolbar: '#tpl_toolbar',
			defaultToolbar: ['filter'],
			height: 'full-100',
			cols: [[
				{type: 'checkbox', title: '全选', id: 'checkAll', width: 48, align: 'center', fixed: 'left'},
				{type: 'numbers', title: '序号', width: 60, align: 'center', fixed: 'left'},
				{
					field: 'name', title: '名称', minWidth: 260, event: 'name', templet: function (d) {
						var icon = d.isdir ? 'bi-folder' : bootstrap_icon(d.ext);

						return '<i class="bi {0} layui-font-20" style="margin-right: 5px;float: left;"></i>{1}'.format(icon, d.name);
					}, sort: true
				},
				{field: 'size', title: '大小', width: 82, align: 'right', sort: true},
				{field: 'downs', title: '下载', width: 82, align: 'center', sort: true},
				{field: 'time', title: '时间', width: 102, align: 'center', sort: true},
				{title: '操作', width: 78, fixed: 'right', align: 'center', templet: '#tpl_action'}
			]],
			page: {
				groups: 1,
				first: '首页',
				last: false,
				layout: ['prev', 'page', 'next', 'skip']
			},
			limit: _conf.type === '' ? 18 : 50,
			parseData: function (res) {
				tableIns.config.page.curr = this.page.curr;
				window._conf.admin = res.admin;
			},
			done: function (res) {
				cut_files(true);
				if (res.path)
					$('#nav').html(function () {
						var navs = ['<a href="#{0}">{1}</a>'.format(buildHash(), '根目录')];
						var ids = [];
						$.each(res.path, function (id, name) {
							if (_conf.type === 'b') ids.push(id);
							else ids = [id];
							navs.push('<a href="#{0}">{1}</a>'.format(buildHash(ids.join('/')), name));
						});
						return navs.join('<span lay-separator="">/</span>')
					});
				if (res.hasOwnProperty('desc')) $('.folder-desc').html(res.desc || '');

				if (res.code < 0) {
					setTimeout(function () {
						parseLayer(res.msg);
					}, 200);
				}
			}
		});
	}

	util.on('lay-on', {
		'login-code': function (othis) {
			$(this).attr('src', 'api.php?c=verify&t=' + Math.random());
		}
	});

	form.on('input-affix', function (data) {
		var elem  = data.elem, // 获取输入框 DOM 对象
			affix = data.affix; // 获取输入框 lay-affix 属性值
		if (affix === 'clear') $(elem).trigger('input');
	});

	function cut_files (id) {
		if (isEmpty(window.cut_file)) window.cut_file = {};

		if (isEmpty(id))
			window.cut_file = {};
		else if (varType(id) === 'Array')
			$.each(id, function (i, v) {
				if (!v.isdir) window.cut_file[v.id] = v.name;
			});
		else if (varType(id) === 'Object')
			window.cut_file = $.extend(window.cut_file, id);

		update_toolbar();
	}

	function update_toolbar () {
		var checkStatus = table.checkStatus('list'),
			checkeds    = checkStatus.data;

		var file = {};
		$.each(checkeds, function (i, v) {
			if (!v.isdir) file[v.id] = v.name;
		});
		var len_checked      = checkeds.length,
			len_checked_file = Object.keys(file).length,
			len_cut          = Object.keys(window.cut_file).length;

		$('.toolbar-admin .copy-link')[len_checked_file > 0 ? 'removeClass' : 'addClass']('layui-hide');
		$('.toolbar-admin [lay-event="cut"]')[len_checked_file > 0 ? 'removeClass' : 'addClass']('layui-hide');
		$('.toolbar-admin [lay-event="paste"]')[len_cut > 0 ? 'removeClass' : 'addClass']('layui-hide')
			.html('<i class="layui-icon layui-icon-release"></i>{0}'.format(
				len_cut > 0 ? '<span class="layui-badge">{0}</span>'.format(len_cut) : ''));
		$('.toolbar-admin [lay-event="delete"]')[len_checked > 0 ? 'removeClass' : 'addClass']('layui-hide');
	}

	table.on('toolbar(list)', function (obj) {
		var event = obj.event,
			d     = obj.data,
			checkStatus, checkeds;
		switch (event) {
			case 'refresh':
				refresh();
				break;
			case 'change':
				parseLayer();
				break;
			case 'folder':
				if (!layerLogin()) return;

				layer.open({
					title: '新建文件夹',
					type: 1,
					skin: 'layer-form',
					shadeClose: true,
					content: $('#create-folder'),
					btn: ['创建', '取消'],
					success: function (elem, index) {
						var that = this;
						elem.find('input[name="pwd"]').show();
						elem.find('input[name],textarea[name]').off('keydown').on('keydown', function (e) {
							if (e.keyCode === 13) that.yes(index, elem);
						}).trigger('input');
					},
					yes: function (index, elem) {
						form.submit('create-folder', function (data) {
							var field = data.field,
								that  = data.elem;

							if (field.name === '') return lianyi.fail('文件夹名称不能为空！');
							if (!/^(\w{2,12})?$/.test(field.pwd)) return lianyi.fail('文件夹密码长度在2到12位之间');

							lianyi.add_icon(that);
							lianyi.request({
								url: 'api.php?c=folder',
								data: $.extend({id: _conf.id}, field),
								msg: '新建文件夹...',
								success: function (res) {
									layer.close(index);
								},
								complete: function (res) {
									lianyi.remove_icon(that);
								}
							});
						});
					},
					end: function () {
						form.val('create-folder', {name: '', pwd: '', desc: ''});
					}
				});
				break;
			case 'cut':
				checkStatus = table.checkStatus('list');
				checkeds = checkStatus.data;

				cut_files(checkeds);
				table.setRowChecked('list', {
					index: 'all',
					checked: false
				});
				update_toolbar();
				break;
			case 'paste':
				if (!layerLogin()) return;

				layer.confirm(Object.values(cut_file).join('<hr>'), {
					title: '确认移动以下文件到此目录',
					shadeClose: true,
					btn: ['移动', '清空', '取消'],
					btn1: function (index) {
						lianyi.request({
							url: 'api.php?c=move',
							data: {id: _conf.id, file_id: Object.keys(cut_file)},
							msg: '移动文件...',
							success: function (res) {
								layer.close(index);
							},
							complete: function (res) {
								cut_files();
							}
						});
						return false;
					},
					btn2: function (index) {
						cut_files();
					}
				});
				break;
			case 'delete':
				if (!layerLogin()) return;

				checkStatus = table.checkStatus('list');
				checkeds = checkStatus.data;

				var folder = {}, file = {};
				$.each(checkeds, function (i, v) {
					if (v.isdir) folder[v.id] = v.name;
					else file[v.id] = v.name;
				});

				layer.confirm(Object.values(folder).concat(Object.values(file)).join('<hr>'), {
					title: '确认删除以下文件（夹）',
					shadeClose: true,
					btn: ['删除', '取消'],
					btn1: function (index) {
						lianyi.request({
							url: 'api.php?c=delete',
							data: {id: _conf.id, file_id: Object.keys(file), folder_id: Object.keys(folder)},
							msg: '删除文件（夹）...',
							success: function (res) {
								layer.close(index);
							}
						});
						return false;
					}
				});
				break;
		}
	});

	table.on('checkbox(list)', function (obj) {
		update_toolbar();
	});

	table.on('tool(list)', function (obj) {
		var d     = obj.data,
			event = obj.event;

		switch (event) {
			case 'name':
				if (d.isdir) {
					if (_conf.type === 'b') {
						return location.hash = location.hash + '/' + d.id;
					}
					return location.hash = d.id;
				}

				if (!REGEX.isVideo(d.name)) return download(buildUrl(d.id + '/' + d.name, {query: d.query}));

				var datas = table.getData('list'),
					urls  = [],
					index = 0;
				$.each(datas, function (i, v) {
					if (v.id === d.id) index = urls.length;
					if (!v.isdir && REGEX.isVideo(v.name))
						urls.push({
							title: v.name,
							url: buildUrl(v.id + '/' + v.name, {query: v.query})
						});
				});
				if (!art_init(urls, index)) download(buildUrl(d.id + '/' + d.name, {query: d.query}));
				break;
			case 'action':
				var disabled = !_conf.admin || _conf.type !== '';
				dropdown.render({
					elem: this, //触发事件的 DOM 对象
					id: 'dropdown-action',
					show: true,
					// trigger: 'hover',
					className: 'dropdown-action',
					align: 'right', //右对齐弹出（v2.6.8 新增）
					style: 'box-shadow: 1px 1px 10px rgb(0 0 0 / 12%);', //设置额外样式
					data: d.isdir ? [
						{
							id: 3,
							title: '设置密码',
							templet: '<i class="bi bi-file-lock layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 4,
							title: '重命名&描述',
							templet: '<i class="bi bi-files-alt layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 9,
							title: '删除',
							templet: '<i class="bi bi-trash layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						}
					] : [
						{id: 1, title: '复制直链1', templet: '<i class="bi bi-link layui-icon-left"></i> {{=d.title}}'},
						{
							id: 2,
							title: '复制直链2',
							templet: '<i class="bi bi-link-45deg layui-icon-left"></i> {{=d.title}}'
						},
						{type: '-'},
						{
							id: 3,
							title: '设置密码',
							templet: '<i class="bi bi-file-lock layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 5,
							title: '文件描述',
							templet: '<i class="bi bi-files-alt layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 6,
							title: '剪贴',
							templet: '<i class="bi bi-scissors layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						},
						{
							id: 9,
							title: '删除',
							templet: '<i class="bi bi-trash layui-icon-left"></i> {{=d.title}}',
							disabled: disabled
						}
					],
					ready: function (elemPanel, elem) {
						var lis = $(elemPanel).find('.layui-dropdown-menu > li.layui-menu-item-divider').prevAll();
						lis.eq(0).unbind('click').attr('data-clipboard-text', buildUrl(d.id + '/' + d.name, {query: d.query}));
						lis.eq(1).unbind('click').attr('data-clipboard-text', buildUrl(d.id + (isEmpty(d.ext) ? '' : '.' + d.ext), {query: d.query}));
					},
					click: function (menudata) {
						switch (menudata.id) {
							case 3:
								if (!layerLogin()) return;

								lianyi.request({
									url: 'api.php?c=info',
									data: {id: d.id, isdir: d.isdir ? 1 : 0},
									msg: '获取数据...',
									success: function (res) {
										layer.prompt(
											{
												title: menudata.title,
												formType: 0,
												shadeClose: true,
												allowBlank: true,
												value: '',
												btn: ['修改', '关闭'],
												success: function (elem) {
													elem.find('.layui-layer-content > .layui-layer-input')
														.attr('placeholder', '请输入新密码')
														.val(res.data.pwd);
												}
											},
											function (value, index, elem) {
												lianyi.add_icon(elem);
												lianyi.request({
													url: 'api.php?c=pwd',
													data: {id: d.id, pwd: value, isdir: d.isdir ? 1 : 0},
													msg: '修改密码...',
													success: function (res) {
														layer.close(index);
													},
													complete: function (res) {
														lianyi.remove_icon(elem);
													}
												});
											}
										);
									}
								});
								break;
							case 4:
								if (!layerLogin()) return;

								lianyi.request({
									url: 'api.php?c=info',
									data: {id: d.id, isdir: 1},
									msg: '获取数据...',
									success: function (res) {
										layer.open({
											title: menudata.title,
											type: 1,
											skin: 'layer-form',
											shadeClose: true,
											content: $('#create-folder'),
											btn: ['修改', '取消'],
											success: function (elem, index) {
												var that = this;
												elem.find('input[name="pwd"]').hide();
												form.val('create-folder', {name: res.data.name, desc: res.data.des});
												elem.find('input[name],textarea[name]').off('keydown').on('keydown', function (e) {
													if (e.keyCode === 13) that.yes(index, elem);
												}).trigger('input');
											},
											yes: function (index, elem) {
												form.submit('create-folder', function (data) {
													var field = data.field,
														that  = data.elem;

													if (field.name === '') return lianyi.fail('文件夹名称不能为空！');
													delete field.pwd;

													lianyi.add_icon(that);
													lianyi.request({
														url: 'api.php?c=rename',
														data: $.extend({id: d.id}, field),
														msg: '修改中...',
														success: function (res) {
															obj.update(field);
															layer.close(index);
														},
														complete: function (res) {
															lianyi.remove_icon(that);
														}
													});
												});
											},
											end: function () {
												form.val('create-folder', {name: '', pwd: '', desc: ''});
											}
										});
									}
								});
								break;
							case 5:
								if (!layerLogin()) return;
								lianyi.request({
									url: 'api.php?c=info',
									data: {id: d.id, isdir: 0},
									msg: '获取数据...',
									success: function (res) {
										layer.prompt(
											{
												title: menudata.title,
												formType: 2,
												shadeClose: true,
												allowBlank: true,
												value: res.data.des,
												btn: ['修改', '关闭'],
												success: function (elem) {
													$(elem).find('.layui-layer-content > .layui-layer-input')
														.attr('placeholder', '请输入文件描述');
												}
											},
											function (value, index, elem) {
												var btn0 = elem.parents('.layui-layer').find('.layui-layer-btn0');
												lianyi.add_icon(btn0);
												lianyi.request({
													url: 'api.php?c=desc',
													data: {id: d.id, desc: value},
													msg: '修改描述...',
													success: function (res) {
														layer.close(index);
													},
													complete: function (res) {
														lianyi.remove_icon(btn0);
													}
												});
											}
										);
									}
								});
								break;
							case 6:
								var file = {};
								file[d.id] = d.name;
								cut_files(file);
								break;
							case 9:
								if (!layerLogin()) return;

								var data = {id: _conf.id};
								data[d.isdir ? 'folder_id' : 'file_id'] = d.id;
								layer.confirm('确认删除文件{0}：<b class="layui-font-red">{1}</b> ？'.format(d.isdir ? '夹' : '', d.name),
									{
										title: '删除文件' + (d.isdir ? '夹' : ''),
										shadeClose: true,
										btn: ['删除', '取消']
									},
									function (index, elem, layero) {
										var btn0 = elem.find('.layui-layer-btn0');
										lianyi.add_icon(btn0);
										lianyi.request({
											url: 'api.php?c=delete',
											data: data,
											msg: '正在删除...',
											success: function (res) {
												layer.close(index);
											},
											complete: function (res) {
												lianyi.remove_icon(btn0);
											}
										});
									}
								);
								break;
						}
					}
				});
				break;
		}
	});

	$('.nav-upload').on('click', function (e) {
		if (!layerLogin()) return;
		var index = layer.open({
			title: '文件上传(中转上传,速度较慢)',
			type: 1,
			maxmin: true,
			skin: 'layer-upload',
			content:
				'  <div class="layui-upload-drag">' +
				'      <i class="layui-icon"></i>' +
				'      <p>点击上传，或将文件拖拽到此处</p>' +
				'  </div>' +
				'<div class="layui-upload-list">' +
				'	<table class="layui-table">' +
				'		<colgroup>' +
				'			<col>' +
				'			<col width="80">' +
				'			<col width="150">' +
				'			<col width="114">' +
				'		</colgroup>' +
				'		<thead>' +
				'		<tr>' +
				'			<th>文件名</th>' +
				'			<th>大小</th>' +
				'			<th>进度</th>' +
				'			<th>操作</th>' +
				'		</tr>' +
				'		</thead>' +
				'		<tbody></tbody>' +
				'	</table>' +
				'</div>' +
				'<div class="layui-upload-btn">' +
				'	<button type="button" class="layui-btn upload-start">上传</button>' +
				'	<button type="button" class="layui-btn layui-btn-danger upload-close" style="float: right">关闭</button>' +
				'</div>'
			,
			success: function (elem, index) {
				$('.layer-upload > .layui-layer-content .upload-close').on('click', function () {
					layer.close(index);
				});
				var upList        = '.layer-upload > .layui-layer-content > .layui-upload-list > table > tbody',
					uploadListIns = upload.render({
						elem: '.layer-upload > .layui-layer-content > .layui-upload-drag',
						url: 'api.php?c=upload',
						data: {id: _conf.id},
						accept: 'file',
						multiple: true,
						drag: true,
						number: 20,
						size: 102400,
						auto: false,
						bindAction: '.layer-upload > .layui-layer-content .upload-start',
						choose: function (obj) {
							var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
							//读取本地文件
							obj.preview(function (index, file, result) {
								var tr = $([
									'<tr id="upload-{0}">'.format(index),
									'<td>{0}</td>'.format(file.name),
									'<td>{0}</td>'.format(size_format(file.size)),
									'<td><div class="layui-progress" lay-filter="progress-demo-{0}"><div class="layui-progress-bar" lay-percent=""></div></div></td>'.format(index),
									'<td><button class="layui-btn layui-btn-xs upload-reload layui-hide">重传</button><button class="layui-btn layui-btn-xs layui-btn-danger upload-delete">删除</button></td>',
									'</tr>'
								].join(''));
								//单个重传
								tr.find('.upload-reload').on('click', function () {
									obj.upload(index, file);
								});

								//删除
								tr.find('.upload-delete').on('click', function () {
									delete files[index]; //删除对应的文件
									tr.remove();
									uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
									$(window).resize();
								});

								$(upList).append(tr);
								element.render('progress'); //渲染新加的进度条组件
								$(window).resize();
							});
						},
						before: function (obj) {
							if (Object.keys(this.files).length <= 0) return false;
							obj.preview(function (index, file, result) {
								var tr  = $(upList).find('tr#upload-' + index),
									tds = tr.children();
								tds.eq(2).html('<div class="layui-progress" lay-filter="progress-demo-{0}"><div class="layui-progress-bar" lay-percent=""></div></div>'.format(index));
								tds.eq(3).find('.upload-reload')
									.addClass('layui-disabled')
									.prop('disabled', true);
							});
							var elem = $('.layer-upload > .layui-layer-content .upload-start');
							if (elem.hasClass('layui-disabled')) return;
							elem.addClass('layui-disabled').prop('disabled', true)
								.prepend('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>');
						},
						done: function (res, index, upload) { //成功的回调
							var tr  = $(upList).find('tr#upload-' + index),
								tds = tr.children();
							if (res.code === 0) { //上传成功
								tds.eq(2).html('<span class="layui-badge layui-bg-green">上传成功</span>');
								tds.eq(3).find('.upload-reload').addClass('layui-hide');
								delete this.files[index]; //删除文件队列已经上传成功的文件
							} else {
								tds.eq(2).html('<span class="layui-badge">{0}</span>'.format(res.msg));
								tds.eq(3).find('.upload-reload').removeClass('layui-hide')
									.removeClass('layui-disabled')
									.prop('disabled', false);
							}
						},
						allDone: function (obj) { //多文件上传完毕后的状态回调
							$('.layer-upload > .layui-layer-content .upload-start')
								.removeClass('layui-disabled').prop('disabled', false)
								.children('.layui-icon-loading').remove();
						},
						error: function (index, upload) { //错误回调
							var tr  = $(upList).find('tr#upload-' + index),
								tds = tr.children();
							tds.eq(3).find('.upload-reload').removeClass('layui-hide')
								.removeClass('layui-disabled')
								.prop('disabled', false); //显示重传
						},
						progress: function (n, elem, e, index) { //注意：index 参数为 layui 2.6.6 新增
							if (n >= 100) {
								var tr  = $(upList).find('tr#upload-' + index),
									tds = tr.children();
								tds.eq(2).html('<span class="layui-badge layui-bg-blue">服务器上传...</span>');
							} else
								element.progress('progress-demo-' + index, n + '%'); //执行进度条。n 即为返回的进度百分比
						}
					});
			}
		});
	});

	$('.nav-admin').on('click', function (e) {
		if (layerLogin())
			return layer.confirm('您已登录，现在退出管理员登录 ？', {
				title: '退出登录',
				shadeClose: true,
				btn: ['退出', '取消'],
				btn1: function (index) {
					lianyi.request({
						url: 'api.php?c=logout',
						msg: '退出登录...',
						success: function (res) {
							layer.close(index);
							window._conf.admin = false;
						}
					});
					return false;
				}
			});
	});

	$(document).on('input propertychange', '.layui-layer .layui-input-wrap input', function () {
		var val = $(this).val();
		switch ($(this).attr('name')) {
			case 'id':
				if (/[^a-zA-Z0-9/]/.test(val)) $(this).val(val.replace(/[^a-zA-Z0-9/]/g, ''));
				break;
			case 'pwd':
				if ($(this).parents('.layui-form').is('#create-folder')) {
					if (/\W/.test(val)) $(this).val(val.replace(/\W+/g, ''));
				} else if (/^\s+|\s+$/.test(val)) $(this).val(val.replace(/^\s+|\s+$/g, ''));
				break;
			case 'pass':
				if (/\s+/.test(val)) $(this).val(val.replace(/\s+/g, ''));
				break;
			case 'code':
				if (/[^0-9a-zA-Z]/.test(val)) $(this).val(val.replace(/[^0-9a-zA-Z]/g, ''));
				break;
			case 'name':
				if (/\s/.test(val)) $(this).val(val.replace(/\s+/g, ''));
				break;
		}
		$(this).prev().children('i').css('color', $(this).val() === '' ? '#d2d2d2' : '#1e9fff');
	});

	$(window).on('hashchange', function (e) {
		parseHash();
		refresh();
	}).trigger('hashchange');

	new ClipboardJS('[data-clipboard-text]').on('success', function (e) {
		dropdown.close('dropdown-action');
		lianyi.success('复制成功');
	}).on('error', function (e) {
		console.log(e);
	});

	new ClipboardJS('.copy-link', {
		text: function (elem) {
			var checkStatus = table.checkStatus('list'),
				checkeds    = checkStatus.data,
				urls        = [];

			$.each(checkeds, function (i, v) {
				if (!v.isdir) urls.push("{0}\t{1}"
					.format(v.name, buildUrl(v.id + '/' + v.name, {query: v.query})));
			});

			if (urls.length === 0) {
				lianyi.fail('无任何文件');
				return '';
			}

			return urls.join("\n");
		}
	}).on('success', function (e) {
		table.setRowChecked('list', {
			index: 'all', // 所有行
			checked: false // 此处若设置 true，则表示全选
		});
		update_toolbar();
		var len = e.text.split('\n').length;
		lianyi.success('成功复制 <b class="layui-font-red">{0}</b> 个直链'.format(len));
	}).on('error', function (e) {
		console.log(e);
	});
});