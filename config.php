<?php

// 相关配置
return [
	// 图形验证码
	'verify' => [
		'type' => 2, // 0:数字, 1:英文字, 2:数字 + 英文字母
		'len'  => 4  // 验证码字符长度
	],

	/* 蓝奏云账号配置，user和cookie只填一个就可，另一个注释掉，cookie优先级高于user */
	'user'   => [
		'uid' => '此处填写您的账号',  // 蓝奏云账号
		'pwd' => '此处填写您的密码'   // 蓝奏云密码
	],
	/*'cookie' => [
		'ylogin'       => '此处填写您的ylogin',      // 蓝奏云用户id
		'phpdisk_info' => '此处填写您的phpdisk_info' // 蓝奏云令牌
	],*/

	'redis'           => false,       // 是否启用redis; true:启用（前提redis服务已开启, php已安装并开启redis扩展）, false:不启用
	'upload_timeout'  => 180,         // 上传文件超时秒数
	'upload_size_max' => 100,         // 允许上传文件大小的最大值(单位: M)

	// 自行修改(重要)
	'admin_pass'      => 'lianyi',                  // 管理员登录密码
	'admin_key'       => 'lianyi',                  // 管理员登录令牌加密秘钥

	'default_pwd' => '00',                      // 默认空密码(2-12位); 当文件夹密码等于此密码时, 表示无密码, 可供游客访问
	'domain'      => 'https://pan.lanzouv.com/' // 默认解析域名
];