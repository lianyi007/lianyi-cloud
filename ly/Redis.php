<?php

namespace ly;

class Redis {
	/**
	 * @var \ly\Redis
	 */
	private static $ins;
	private $opt = [
		'host'     => '127.0.0.1',
		'port'     => 6379,
		'password' => '',
		'index'    => 0,
		'prefix'   => ''
	];

	private $redis;

	/**
	 * @param  array{host:string,port:int,password:string,prefix:string,select:int}  $opt
	 *
	 * @throws \Exception
	 */
	private function __construct ($opt = []) {
		if (!self::enable())
			throw new \Exception('未开启redis扩展');

		$this->opt = array_merge($this->opt, array_filter($opt));

		$this->redis = new \Redis();
		$this->redis->connecT($this->opt['host'], $this->opt['port']);
		$pass = strval($this->opt['password']);
		if ($pass !== '') $this->redis->auth($pass);

		$index = intval($this->opt['index']);
		if ($index > 0) $this->redis->select($index);
		$this->redis->setOption(\Redis::OPT_PREFIX, $this->opt['prefix']);
	}

	public static function enable () {
		return extension_loaded('Redis');
	}

	/**
	 * redis 实例生成
	 *
	 * @param  array{host:string,port:int,password:string,prefix:string,select:int}  $opt
	 * @param  bool                                                                  $renew
	 *
	 * @return Redis
	 * @throws \Exception
	 */
	public static function instance ($opt = ['prefix' => '', 'select' => 0], $renew = true) {
		if (strval(@$opt['prefix']) !== '' && substr($opt['prefix'], -1) !== ':') $opt['prefix'] .= ':';
		try {
			if (!(self::$ins instanceof self) || $renew) {
				self::$ins = new self($opt);
			} else {
				$index = intval($opt['index']);
				if (self::$ins->opt['index'] !== $index)
					self::$ins->redis->select($index);
				$prefix = strval($opt['prefix']);
				if (self::$ins->opt['prefix'] !== $prefix)
					self::$ins->redis->setOption(\Redis::OPT_PREFIX, $prefix);

				self::$ins->opt = array_merge(self::$ins->opt, $opt);
			}

			return self::$ins;
		} catch (\Exception $e) {
			exit(jsonMsg(-100, 'Redis 连接失败:' . $e->getMessage()));
		}
	}

	/**
	 * @param  string  $prefix
	 * @param  int     $index
	 *
	 * @return \ly\Redis
	 * @throws \Exception
	 */
	public static function prefix ($prefix, $index = 0) {
		return self::select($index, $prefix);
	}

	/**
	 * @param  int     $index
	 * @param  string  $prefix
	 *
	 * @return \ly\Redis
	 * @throws \Exception
	 */
	public static function select ($index, $prefix = '') {
		return self::instance(['prefix' => strval($prefix), 'select' => intval($index)], false);
	}

	/**
	 * 设置值  构建一个字符串
	 *
	 * @param  array|string  $key     KEY名称
	 * @param  mixed         $value   值
	 * @param  int           $expire  过期时间  -1表示无过期时间
	 *
	 * @return boolean
	 * @throws \RedisException
	 */
	public function set ($key, $value, $expire = -1) {
		$key = $this->convertKey($key);

		return $expire > 0 ? $this->redis->setex($key, $expire, $value) : $this->redis->set($key, $value);
	}

	/**
	 * @param  array|string  $key
	 * @param  mixed         $value
	 *
	 * @return array|bool|\Redis
	 * @throws \RedisException
	 */
	public function setnx ($key, $value) {
		$key = $this->convertKey($key);

		return $this->redis->setnx($key, $value);
	}

	/**
	 * @param  array|string  $key
	 * @param  int           $expire
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function lock ($key, $expire = 5) {
		if (!is_array($key)) $key = [$key];
		array_unshift($key, 'lock');
		$key = $this->convertKey($key);

		return true === $this->redis->set($key, 1, ['EX' => $expire, 'NX']);
	}

	/**
	 * @param  array|string  $key
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function unlock ($key) {
		if (!is_array($key)) $key = [$key];
		array_unshift($key, 'lock');
		$key = $this->convertKey($key);

		return false !== $this->redis->del($key);
	}

	/**
	 * 修改key过期时间
	 *
	 * @param  array|string  $key
	 * @param  int           $expiry
	 *
	 * @return $this
	 * @throws \RedisException
	 */
	public function expire ($key, $expiry = -1) {
		$this->redis->expire($this->convertKey($key), $expiry);

		return $this;
	}

	/**
	 * 获取key剩余时间（秒）
	 *
	 * @param  array|string  $key
	 *
	 * @return bool|int
	 * @throws \RedisException
	 */
	public function ttl ($key) {
		return $this->redis->ttl($this->convertKey($key));
	}

	/**
	 * 获取key剩余时间（毫秒）
	 *
	 * @param  array|string  $key
	 *
	 * @return bool|int
	 * @throws \RedisException
	 */
	public function pttl ($key) {
		return $this->redis->pttl($this->convertKey($key));
	}

	/**
	 * 构建一个集合(无序集合)
	 *
	 * @param  array|string  $key       集合键名
	 * @param  mixed         ...$value  值
	 *
	 * @return bool|int
	 */
	public function sadd ($key, ...$value) {
		return $this->redis->sadd($this->convertKey($key), ...$value);
	}

	/**
	 * 构建一个集合(无序集合)
	 *
	 * @param  array|string  $key    集合键名
	 * @param  array         $value  值
	 *
	 * @return bool|int
	 * @throws \RedisException
	 */
	public function sAddArray ($key, $value) {
		return $this->redis->sAddArray($this->convertKey($key), $value);
	}

	/**
	 * 构建一个集合(有序集合)
	 *
	 * @param  array|string  $key       集合名称
	 * @param  array|float   $score
	 * @param  mixed         ...$value  值
	 *
	 * @return false|int
	 * @throws \RedisException
	 */
	public function zadd ($key, $score, ...$value) {
		return $this->redis->zadd($this->convertKey($key), $score, ...$value);
	}

	/**
	 * 取集合对应元素
	 *
	 * @param  array|string  $key  集合名字
	 *
	 * @return array
	 * @throws \RedisException
	 */
	public function smembers ($key) {
		return $this->redis->smembers($this->convertKey($key));
	}

	/**
	 * 构建一个列表(先进后去，类似栈)
	 *
	 * @param  array|string  $key    KEY名称
	 * @param  string        $value  值
	 *
	 * @return false|int
	 * @throws \RedisException
	 */
	public function lPush ($key, $value) {
		return $this->redis->lPush($this->convertKey($key), $value);
	}

	/**
	 * 构建一个列表(先进先去，类似队列)
	 *
	 * @param  array|string  $key    KEY名称
	 * @param  string        $value  值
	 *
	 * @return bool|int
	 * @throws \RedisException
	 */
	public function rPush ($key, $value) {
		return $this->redis->rPush($this->convertKey($key), $value);
	}

	/**
	 * 获取所有列表数据（从头到尾取）
	 *
	 * @param  array|string  $key   KEY名称
	 * @param  int           $head  开始
	 * @param  int           $tail  结束
	 *
	 * @return array
	 * @throws \RedisException
	 */
	public function lRange ($key, $head, $tail) {
		return $this->redis->lRange($this->convertKey($key), $head, $tail);
	}

	/**
	 * HASH类型
	 *
	 * @param  array|string  $key    表名字key
	 * @param  string        $hashKey
	 * @param  string        $value  值
	 *
	 * @return bool|int
	 * @throws \RedisException
	 */
	public function hSet ($key, $hashKey, $value) {
		return $this->redis->hSet($this->convertKey($key), $hashKey, $value);
	}

	/**
	 * @param  array|string  $key
	 * @param  string        $hashKey
	 *
	 * @return false|\Redis|string
	 * @throws \RedisException
	 */
	public function hGet ($key, $hashKey) {
		return $this->redis->hGet($this->convertKey($key), $hashKey);
	}

	/**
	 * 通过key获取数据
	 *
	 * @param  array|string  $keys   KEY名称
	 * @param  bool          $multi  是否获取多个key
	 *
	 * @return false|false[]|mixed|\Redis|string|string[]
	 * @throws \RedisException
	 */
	public function get ($keys, $multi = false) {
		if (!$multi) return $this->redis->get($this->convertKey($keys));

		$keys = array_map(function ($v) {
			return $this->convertKey($v);
		}, $keys);

		return $this->redis->mget($keys);
	}

	/**
	 * 获取所有匹配的key名，不是值
	 *
	 * @param  string  $rule  dos表达式规则
	 *
	 * @return array
	 * @throws \RedisException
	 */
	public function keys ($rule = '*') {
		return $this->redis->keys($this->convertKey($rule));
	}

	/**
	 * 删除key数据
	 *
	 * @param  array|string  $key
	 *
	 * @return int
	 * @throws \RedisException
	 */
	public function del ($key) {
		return $this->redis->del($this->convertKey($key));
	}

	/**
	 * 数据自增
	 *
	 * @param  array|string  $key  KEY名称
	 * @param  int           $num  自增的数值
	 *
	 * @return int
	 * @throws \RedisException
	 */
	public function incr ($key, $num = 1) {
		return $this->redis->incr($this->convertKey($key), $num);
	}

	/**
	 * 数据自减
	 *
	 * @param  array|string  $key  KEY名称
	 * @param  int           $num  自减的数值
	 *
	 * @return int
	 * @throws \RedisException
	 */
	public function decr ($key, $num = 1) {
		return $this->redis->decr($this->convertKey($key), $num);
	}

	/**
	 * 判断key是否存在
	 *
	 * @param  array|string  $key  KEY名称
	 *
	 * @return boolean
	 * @throws \RedisException
	 */
	public function exists ($key) {
		return $this->redis->exists($this->convertKey($key));
	}

	/**
	 * 重命名key
	 *
	 * @param  array|string  $key      原key
	 * @param  string        $new_key  新key
	 *
	 * @return bool|\Redis
	 * @throws \RedisException
	 */
	public function rename ($key, $new_key) {
		return $this->redis->rename($this->convertKey($key), $this->convertKey($new_key));
	}

	/**
	 * 重命名- 当且仅当$new_key不存在时，将key改为$new_key ，当$new_key存在时候会报错
	 *  和 rename不一样，它是直接更新（存在的值也会直接更新）
	 *
	 * @param  array|string  $key      原key
	 * @param  string        $new_key  新key
	 *
	 * @return bool
	 * @throws \RedisException
	 */
	public function renameNx ($key, $new_key) {
		return $this->redis->renameNx($this->convertKey($key), $this->convertKey($new_key));
	}

	/**
	 * 获取KEY存储的值类型
	 * none(key不存在) int(0)  string(字符串) int(1)   list(列表) int(3)  set(集合) int(2)   zset(有序集) int(4)    hash(哈希表) int(5)
	 *
	 * @param  array|string  $key  KEY名称
	 *
	 * @return int
	 * @throws \RedisException
	 */
	public function type ($key) {
		return $this->redis->type($this->convertKey($key));
	}

	/**
	 * 清空数据
	 *
	 * @return bool|\Redis
	 * @throws \RedisException
	 */
	public function flushAll () {
		return $this->redis->flushAll();
	}

	public function __clone () {
		trigger_error('Clone is not allow!', E_USER_ERROR);
	}

	/**
	 * 返回redis对象
	 * redis有非常多的操作方法，这里只封装了一部分
	 * 拿着这个对象就可以直接调用redis自身方法
	 * eg:$redis->redisOtherMethods()->keys('*a*')   keys方法没封
	 */
	public function redisOtherMethods () {
		return $this->redis;
	}

	/**
	 * 根据key所设定的前缀转化为真实key
	 *
	 * @param  array|string  $keys
	 *
	 * @return string
	 */
	private function convertKey ($keys, $separator = ':') {
		if (!is_array($keys)) $keys = [$keys];
		$keys = array_filter($keys);

		return implode($separator, array_map(function ($v) use ($separator) {
			return str_replace(['/', '\\'], $separator, $v);
		}, $keys));
	}
}