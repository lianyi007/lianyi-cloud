# 涟漪云 v0.3.1

#### 介绍

蓝奏云列表及解析程序，自由操作蓝奏云内文件（夹），并可解析直链下载等等。  
演示：[查看演示](https://lanzou.ly93.cc)，讨论交流联系QQ：29397395

#### 更新说明

1. 支持直接配置账号密码
2. 重构部分函数名
3. 修复管理登录界面错位bug
4. 修复js中部分代码异常
5. 修复部分情况下解析分享的目录中部分文件需要pwd但只有webpage参数（临时短效加密密码）导致解析异常（如分享id：b02en2kxa，密码：7L86，此时直链有时效性）
6. 增加和修改部分可配置参数（查看Lanzou.php文件中$conf）


#### 大体功能如下

1. 浏览任意目录内文件（夹）
2. 批量移动文件
3. 批量删除文件（夹）
4. 重命名文件夹
5. 新建文件夹
6. 文件夹加密、描述及修改
7. 文件批量上传
8. 文件直链  

| 结构       | 说明          | 示例                                                                            |
|----------|-------------|-------------------------------------------------------------------------------|
| id+name  | 域名/id/name  | https://lanzou.ly93.cc/183401467/音乐冢_4.0.2.apk                                |
| id       | 域名/id       | https://lanzou.ly93.cc/183401467 或 https://lanzou.ly93.cc/183401467.apk       |
| sid+name | 域名/sid/name | https://lanzou.ly93.cc/iBL8n205wg6h/音乐冢_4.0.2.apk                             |
| sid      | 域名/sid      | https://lanzou.ly93.cc/iBL8n205wg6h 或 https://lanzou.ly93.cc/iBL8n205wg6h.apk |

备注
| 变量   | 说明     |
|------|--------|
| id   | 文件id   |
| sid  | 文件分享id |
| name | 文件名称   |

#### 软件架构

1. PHP >= 5.6 (开启redis扩展)
2. Redis

#### 安装教程

1. 下载源码
2. 将源码上传至你的网站根目录并解压
3. 修改配置文件(config.php)相关配置，user和cookie必填其中1个，cookie优先级高于user，如果填写user就可跳过步骤4直接进行步骤5
4. 获取cookie(浏览器F12控制台执行)：
   ```javascript
   if(!/(^|\.)woozooo\.com$/i.test(document.location.host))
	   throw new Error('请登录到蓝奏云控制台在执行此代码！');
   
   var regex = /(?<=^|;)\s*([^=]+)=\s*(.*?)\s*(?=;|$)/g,
	   cookies = {},re;
   while(re = regex.exec(document.cookie))
	   if(re[1] === 'ylogin'||re[1] === 'phpdisk_info')
		   cookies[re[1]] = re[1]+'='+re[2]+';';
   
   if(!cookies.hasOwnProperty('phpdisk_info'))
	   throw new Error('获取cookie失败，请确认您已登录到蓝奏云控制台！');
   
   (function (str) {
	   var oInput = document.createElement('input');
	   oInput.value = str;
	   document.body.appendChild(oInput);
	   oInput.select();
	   document.execCommand("Copy");
	   oInput.remove();
	   alert('复制成功');
   })(Object.values(cookies).join(' '));
   ```
   配置cookie（phpdisk_info=xxx; ylogin=yyy;）xxx、yyy填写到对应区域
5. 配置伪静态  
	Nginx：
	```nginx
	if (!-e $request_filename) {
		rewrite ^/([1-9]\d*|i[a-zA-Z0-9]+)(\.[\w]+|/([^/]+))?$ /api.php?id=$1&name=$3 break;
	}
	```
	Apache：
	```apache
	RewriteEngine On

	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule ^([1-9]\d*|i[a-zA-Z0-9]+)(\.\w+|/([^/]+))?$ /api.php?id=$1&name=$3 [QSA,L]
	```